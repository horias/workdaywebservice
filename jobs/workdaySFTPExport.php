<?php 
require_once __DIR__.'/../eccommon/php/autoload.php';
//archive();
export();

function archive() {    
    $Ixsys_config = new Ixsys_config();
    $sftp = $Ixsys_config->init_sftp_connection();
    if (!($files = $sftp->nlist("/out", true)))
{
    die("Cannot read directory contents");
}

foreach ($files as $file)
{
    echo "$file\n";
}
}
function export() {
    //something in autoload adds 1 extra empty line to response, making the xml invalid to the DLL.
    //require_once __DIR__.'/../eccommon/php/autoload.php';
    //require_once './config/ixsys_config.php';
    $Ixsys_config = new Ixsys_config();
    $dbc = $Ixsys_config->init_axalta_db();
    $row_array = array();
    $client = "workdayJHCC";
    $jhccFtpExported = false;

    //getting all eligible invoice numbers (that haven't already been exported)
    $sqlhead = "SELECT hinvc, hstore from ecshead_web WHERE client = ? AND jhccFtpExported = ?";    
    $stmt = $dbc->prepare($sqlhead);    
    $stmt->bind_param('ss', $client, $jhccFtpExported);
    $stmt->execute();  
    $reshead = $stmt->get_result();    
    if ($reshead) {           
        while ($row = mysqli_fetch_array($reshead, MYSQLI_ASSOC)) {
            array_push($row_array, $row);             
        }
    }
    $count = count($row_array);
    echo "Starting export for ".$count." invoices";
    $exported = 0;
    foreach ($row_array as $key => $invoice) {
        $EDI = generate_EDI_file($invoice['hinvc'], $invoice['hstore']);
        $fileName = date("Ymd")."-".$invoice['hinvc'].".txt";
        $resExport = export_EDI_file($EDI, $fileName);
        if($resExport) {
            $exported+=1;
            $msg = "Exported invoice #".strval($invoice['hinvc']);
            $sql = "UPDATE ecshead_web SET jhccFtpExported = ? WHERE hinvc = ?";
            $stmt = $dbc->prepare($sql);
            $jhccFtpExported = true;
            $stmt->bind_param('is', $jhccFtpExported, $invoice['hinvc'] );
            $stmt->execute();
            WriteToLog($msg);        
            echo $msg;    
        }
        else {
            $msg = "Failed to export invoice #".strval($invoice['hinvc']."- ERROR: ".$resExport);
            WriteToLog($msg);
            echo $msg;
        }
    }
    if ($exported == $count) {
        return true;
    }
    else {
        return false;
    }
}

function export_EDI_file($EDI, $fileName) {
    $Ixsys_config = new Ixsys_config();
        $sftp = $Ixsys_config->init_test_sftp_connection();    
        $sftp->chdir('out');
        if ($sftp->put($fileName, $EDI))   {
            file_put_contents("./uploaded/".$fileName, $EDI);
            return true;
        }
        else {
            file_put_contents("./failed/".$fileName, $EDI);
            return $sftp->getLastSFTPError();
        }
}

function generate_EDI_file($hinvc, $hstore) { 
    //something in autoload adds 1 extra empty line to response, making the xml invalid to the DLL.
    //require_once __DIR__.'/eccommon/php/autoload.php';
    //require_once './config/ixsys_config.php'; 
    $invoiceData = array();
    $invoiceData['head'] = getHead($hinvc, $hstore);
    $invoiceData['tran'] = getTran($hinvc, $hstore);
    
    //file_put_contents("source.txt", json_encode($invoiceData));
    $ISA_01 = "00"; // I01 - Authorization information qualifier - 2 Char
    $ISA_02 = substr(str_pad("",10," ",STR_PAD_RIGHT),0,10); // I02 - Authorization information - 10 Char
    $ISA_03 = "00"; // I03 - Security information qualifier - 2 Char
    $ISA_04 = substr(str_pad("",10," ",STR_PAD_RIGHT),0,10); // I04 - Security information - 10 Char
    $ISA_05 = "ZZ"; // I05 - Interchange ID qualifier - 2 Char
    $ISA_06 = substr(str_pad("05-013-9922",15," ",STR_PAD_RIGHT),0,15); // I06 - Interchange sender ID - 15 Char - EC DUNS Number
    $ISA_07 = "01"; // I05 - Interchange ID qualifier - 2 Char
    $ISA_08 = substr(str_pad("84-987-0118",15," ",STR_PAD_RIGHT),0,15); // I07 - Interchange receiver ID - 15 Char - Joe Hudson DUNS Number
    $ISA_09 = date("ymd"); // I08 - Interchange date - 6 Char
    $ISA_10 = date("Hi"); // I09 - Interchange time - 4 Char
    $ISA_11 = "U"; // I65 - Repetition Separator - 1 Char
    $ISA_12 = "00801"; // I11 - Interchange Control Version Number Code - 5 Char
    $ISA_13 = "100009999"; // I12 - Interchange Control Number - 9 Char
    $ISA_14 = "0"; // I13 - Acknowledgement Requested Code - 1 Char
    $ISA_15 = "T"; // I14 - Interchange Usage Indicator Code - 1 Char
    $ISA_16 = "^"; //I15 - Component Element Separator - 1 Char
    $EDI = "ISA*".$ISA_01."*".$ISA_02."*".$ISA_03."*".$ISA_04."*".$ISA_05."*".$ISA_06."*".$ISA_07."*".$ISA_08."*".$ISA_09."*".$ISA_10."*".$ISA_11."*".$ISA_12."*".$ISA_13."*".$ISA_14."*".$ISA_15."*".$ISA_16."~";
    $GS_01 = "IN"; // 479 - Functional Identifier Code - 2 Char
    $GS_02 = substr(str_pad("05-013-9922",2," ",STR_PAD_RIGHT),0,15); // 142 - Application Sender's Code - 2-15 Char - EC DUNS Number
    $GS_03 = substr(str_pad("84-987-0118",2," ",STR_PAD_RIGHT),0,15); // 142 - Application Receiver's Code - 2-15 Char - Joe Hudson DUNS Number
    $GS_04 = date("Ymd"); // 373 - Date - 8 Char
    $GS_05 = date("His");// 337 - Time - 4-8 Char
    $GS_06 = substr(str_pad("395611",1," ",STR_PAD_RIGHT),0,9); // 28 - Group Control Number - 1-9 Char
    $GS_07 = substr(str_pad("X",1," ",STR_PAD_RIGHT),0,2); // 455 - Responsible Agency Code - 1-2 Char
    $GS_08 = substr(str_pad("008010",1," ",STR_PAD_RIGHT),0,12); // 480 - Version/Release/Industry Identifier Code - 1-12 Char
    $EDI.= "GS*".$GS_01."*".$GS_02."*".$GS_03."*".$GS_04."*".$GS_05."*".$GS_06."*".$GS_07."*".$GS_08."~";
    $ST_01 = "810";
    $ST_02 = "395611"; // 329 - Transaction set control number - 4-9 Char
    $EDI.= "ST*".$ST_01."*".$ST_02."~";
    $BIG_01 = date("Ymd", strtotime($invoiceData['head']['hdate'])); // 373 - Date - 8 Char
    $BIG_02 = substr(str_pad($invoiceData['head']['hinvc'],1," ",STR_PAD_RIGHT),0,22); // 76 - Invoice Number - 1-22 Char
    $BIG_07 = "CI"; // 640 - Transaction Type Code = CI = Consolidated Invoice
    $EDI.= "BIG*".$BIG_01."*".$BIG_02."*****".$BIG_07."~";   
    $NTE_01 = ""; // 363 - Note Reference Code - 3 Char
    $NTE_02 = substr(str_pad($invoiceData['head']['hcomm'],1," ",STR_PAD_RIGHT),0,80); // 352 - Description - 1-80 Char
    $EDI.="NTE*".$NTE_01."*".str_replace("*", "--", $NTE_02)."~";
    $costCenter = getCostCenter($invoiceData['head']['hcust']);
    foreach ($invoiceData['tran'] as $i => $line ) {
        /*$IT1_01 = $i+1; // 350 - Assigned Identification - 1-20 Char
        $IT1_02 = $line['tqty']; // 358 - Quantity Invoiced - Supplier Units - 1-15 Char
        $IT1_03 = "EA"; // 355 Unit or Basis for Measuerment Code - EA = EACH - 2 Char
        $IT1_04 = $line['tprc']; // 212 - Unit Price - 1-17 Char
        $IT1_06 = "E5"; // 235 - E5 = Tax Code
        $IT1_07 =  $line['tax']; // 235
        $IT1_10 = "VN"; // 235 - Code identifying the type/source of the descriptive number used in Product/Service ID - VN = Vendor's Item Number - 2 Char
        $IT1_11 = $line['tpart']; // 234 - Product/Service ID - 1-80 Char
        $IT1_12 = "PD"; // 235 - Code identifying the type/source of the descriptive number used in Product/Service ID - PD = Part Number Description - 2 Char
        $IT1_13 = $line['tdesc']; // 234 - Product/Service ID - 1-80 Char*/

        $IT1_01 = $i+1; // 350 - Assigned Identification - 1-20 Char
        $IT1_02 = $line['tqty']; // 358 - Quantity Invoiced - Supplier Units - 1-15 Char
        $IT1_03 = "EA"; // 355 Unit or Basis for Measuerment Code - EA = EACH - 2 Char
        $IT1_04 = $line['tprc']; // 212 - Unit Price - 1-17 Char
        $IT1_08 = "ZZ"; // 235 - Code identifying the type/source of the descriptive number used in Product/Service ID - ZZ = Mutually Defined - 2 Char
        $IT1_09 = $line['tmfg']."-".$line['tpart']."-".$line['tsize']; // 234 - Product/Service ID - 1-80 Char        
        $EDI.="IT1*".$IT1_01."*".$IT1_02."*".$IT1_03."*".$IT1_04."****".$IT1_08."*".$IT1_09."~";
        $PID_01 = "F";
        $PID_05 = $line['tdesc'];
        $EDI.="PID*".$PID_01."****".$PID_05."~";
        $REF_01_1 = "CA";
        $REF_03_1 = $costCenter; //Cost Center
        $EDI.="REF*".$REF_01_1."**".$REF_03_1."~";
        
        $REF_01_2 = "ZZ";
        $spendCode = getProductSpendCode($line['tmfg'],$line['tpart'],$line['tsize']);
        if (trim($spendCode) !== "") {
            $REF_02_2 = $spendCode;//Spent Category
        }
        else {
            $REF_02_2 = getCategorySpendCode($line['voctyp']);//Spent Category
        } 
        $EDI.="REF*".$REF_01_2."*".$REF_02_2."~";


    //$EDI.= "IT1*".$i."*".$line['tqty']."*EA*".$line['tprc']."******VN*".$line['tpart']."**~";
    }
    $hisub = $invoiceData['head']['hisub'];
    $hisubcents = floatval($hisub)*100;
    $TDS_01 = strval($hisubcents);
    $EDI.= "TDS*".$TDS_01."~"; // Use this for hisub
    //$EDI.="AMT*1*".$hisub."~"; // Or use this for hisub
    $TXI_01 = "ST"; // 963 - Tax Type Code - 2 Char - State Tax
    $TXI_02 = strval($invoiceData['head']['hstax']); // 782 - Monetary Amount
    $EDI.="TXI*".$TXI_01."*".$TXI_02."~";
    $SE_01 = "1"; // 96 - Number of segments included
    $SE_02 = $ST_02;// 329 - Transaction set control number
    $EDI.= "SE*".$SE_01."*".$SE_02."~";
    $GE_01 = "1"; // 97 - Number of transaction sets included - 1-6 Char
    $GE_02 = $GS_06; // 28 - Group Control Number - 1-9 Char
    $EDI.= "GE*".$GE_01."*".$GE_02."~";
    $IEA_01 = "1"; // I16 - Number of Included Functional Groups - 1-5 Char
    $IEA_02 = $ISA_13; // I12 - Interchange Control Number - 9 Char
    $EDI.="IEA*".$IEA_01."*".$IEA_02."~";

    return $EDI;
    
    
    //echo json_encode($sftp->lstat('editext.txt'));
    //echo json_encode($sftp->nlist('.', true));
    //$sftp->get('editext.txt', 'downloaded.txt');
    //echo $sftp->getSFTPLog();
}

function archiveFiles() {
    $Ixsys_config = new Ixsys_config();
    $sftp = $Ixsys_config->init_sftp_connection();
    $dir = "/out/";
    $files = $sftp->nlist($dir, true);
    foreach($files as $file)
    {   
        if ($file == '.' || $file == '..') continue;
        $sftp->put($dir."archive".'/'.$file, $sftp->get($dir.'/'.$file)); 
    }
}

function getCostCenter($custNo) {
    //something in autoload adds 1 extra empty line to response, making the xml invalid to the DLL.
    //require_once __DIR__.'/eccommon/php/autoload.php';
    //require_once './config/ixsys_config.php';
    $Ixsys_config = new Ixsys_config();
    $dbc = $Ixsys_config->init_axalta_db();
    $sql = "SELECT * from joe_hudson_locations WHERE custNo = ?";    
    $stmt = $dbc->prepare($sql);
    $stmt->bind_param('i', $custNo);
    $stmt->execute();   
    $res = $stmt->get_result();
    if ($res) {           
        while ($row = mysqli_fetch_array($res, MYSQLI_ASSOC)) {               
            $costCenter = $row['workdayID'];             
        }
    }
    return $costCenter;
}

function getCategorySpendCode($voctyp) {
    $retval = "SC001";
    return $retval;
    //something in autoload adds 1 extra empty line to response, making the xml invalid to the DLL.
    //require_once __DIR__.'/eccommon/php/autoload.php';
    //require_once './config/ixsys_config.php';
    $Ixsys_config = new Ixsys_config();
    $dbc = $Ixsys_config->init_axalta_db();
    $sql = "SELECT * from joe_hudson_category_spend_codes WHERE type = ?";    
    $stmt = $dbc->prepare($sql);
    $stmt->bind_param('s', $voctyp);
    $stmt->execute();   
    $res = $stmt->get_result();
    if ($res) {           
        while ($row = mysqli_fetch_array($res, MYSQLI_ASSOC)) {               
            $spendCode = $row['code'];             
        }
    }
    return $spendCode;
}

function getProductSpendCode($mfg, $part, $size) {
    //something in autoload adds 1 extra empty line to response, making the xml invalid to the DLL.
    //require_once __DIR__.'/eccommon/php/autoload.php';
    //require_once './config/ixsys_config.php';
    $Ixsys_config = new Ixsys_config();
    $dbc = $Ixsys_config->init_axalta_db();
    $spendCode = "";
    $sql = "SELECT * from joe_hudson_product_spend_codes WHERE mfg = ? AND part = ? AND size = ?";    
    $stmt = $dbc->prepare($sql);
    $stmt->bind_param('sss', $mfg, $part, $size);
    $stmt->execute();   
    $res = $stmt->get_result();
    if ($res) {           
        while ($row = mysqli_fetch_array($res, MYSQLI_ASSOC)) {               
            $spendCode = $row['code'];             
        }
    }
    return $spendCode;
}

function getHead($hinvc, $hstore) {
    //something in autoload adds 1 extra empty line to response, making the xml invalid to the DLL.
    require_once __DIR__.'/../eccommon/php/autoload.php';
    //require_once './config/ixsys_config.php';
    $Ixsys_config = new Ixsys_config();
    $dbc = $Ixsys_config->init_axalta_db();
    $row_array = array();
    $sqlhead = "SELECT * from ecshead_web WHERE hinvc = ? AND hstore = ?";    
    $stmt = $dbc->prepare($sqlhead);
    
    $stmt->bind_param('is', $hinvc, $hstore);
    $stmt->execute();   
    $reshead = $stmt->get_result();
    if ($reshead) {           
        while ($row = mysqli_fetch_array($reshead, MYSQLI_ASSOC)) {   
            $row_array = $row;             
        }
    }
    return $row_array;
}
function getTran($hinvc, $hstore) {
    //something in autoload adds 1 extra empty line to response, making the xml invalid to the DLL.
    require_once __DIR__.'/../eccommon/php/autoload.php';
    //require_once './config/ixsys_config.php';
    $Ixsys_config = new Ixsys_config();
    $dbc = $Ixsys_config->init_axalta_db();
    $row_array = array();
    $sqltran = "SELECT a.*,b.voctyp
                from ecstran_web a 
                join psitem b 
                on a.tpart = b.pspart  AND CONVERT(a.tsize, INT) = CONVERT(b.pssize, INT) AND a.tmfg = b.psmfg 
                where a.tinvc = ? AND a.tstore = ?";
    $stmt = $dbc->prepare($sqltran);
    $stmt->bind_param('is', $hinvc, $hstore);
    $stmt->execute();
    $restran = $stmt->get_result();
    if ($restran) {           
        while ($row = mysqli_fetch_array($restran, MYSQLI_ASSOC)) {
            array_push($row_array, $row);             
        }
    }
    return $row_array;
}
function writeToLog($message = ""){
    $myfile = fopen("workday_sftp_log.txt", "a") or die("Unable to open file!");
    $txt = $message."\r\n";
    fwrite($myfile, $txt);
    fclose($myfile);
    
}


?>