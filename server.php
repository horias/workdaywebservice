<?php
require_once __DIR__.'/eccommon/php/autoload.php';
/*use phpseclib3\Net\SFTP;
use phpseclib3\Crypt\PublicKeyLoader;
define('NET_SFTP_QUEUE_SIZE', 1);
define('NET_SFTP_UPLOAD_QUEUE_SIZE', 1);
define('NET_SFTP_LOGGING', 2);
$Ixsys_config = new Ixsys_config();
$testHinvc = $Ixsys_config->$invoice_to_test;
$testHstore = $Ixsys_config->$store_to_test;*/
//generate_EDI_file($hinvc, $hstore);*/
function doAuthenticate()   {
    $ini = parse_ini_file('../config/webservice.ini');
    if (isset($_SERVER['PHP_AUTH_USER']) and isset($_SERVER['PHP_AUTH_PW'])) 
    { 
        if ($_SERVER['PHP_AUTH_USER'] == $ini['nexsyisWebAuthUsr'] && $_SERVER['PHP_AUTH_PW'] == $ini['nexsyisWebAuthPwd'])
            return true;
        else
            return false;
    }
}
function doAuthenticateApplet($username, $password) {
    $ini = parse_ini_file('../config/webservice.ini');
    if (isset($username) and isset($password)) 
    { 
        if ($username == $ini['applAuthUsr'] && $password == $ini['applAuthPwd'])
            return true;
        else
            return false;
    }
}

function uploadInvoice($username, $password, $ecshead, $ecstran)
    {   
        if (!doAuthenticateApplet($username, $password))
        {
            $error_message = " - Invalid SOAP username or password. Please check Soap Client credentials.";
            return json_encode($error_message);
        }
        $ecsheadXML = simplexml_load_string($ecshead) or die ("Error: Cannot Create Object");
        $ecstranXML = simplexml_load_string($ecstran) or die ("Error: Cannot Create Object");
        $headJson = json_encode($ecsheadXML, true);
        $tranJson = json_encode($ecstranXML, true);   
        $aHead = json_decode($headJson, true);
        $aTran = json_decode($tranJson, true);        
        $insertedHead = 0;
        $insertedTran = 0;
        $bRetValHead = insertHead($aHead);
        
        //this is for fixing orders that only have one line and are converted into a simple json array; orders with multiple lines are a list array.
        $adjustedATran = array();
        $adjustedAHead = array();
        if (count($aTran['ecstran'][0]) == 0)
            $adjustedATran[0] = $aTran['ecstran']; 
        else 
            $adjustedATran = $aTran['ecstran']; 
        /*(file_put_contents("aHead.log", json_encode($aHead));
        file_put_contents("aTran.log", json_encode($aTran));
        file_put_contents("adjustedAHead.log", json_encode($adjustedAHead));
        file_put_contents("adjustedATran.log", json_encode($adjustedATran));*/
        $bRetValTran = insertTran($adjustedATran);
        return $bRetValTran;
        if ($bRetValTran && $bRetValHead) {
            writeToLog ("Inserted invoice no: " . $aHead['ecshead']['hinvc'], "invoice_uploads.log");
            return true;    
        }
        else {
            //revertCurrentUpload($aHead['ecshead']['hinvc']);
            return "reverting";
        } 
        
        
    }

function revertCurrentUpload($orderId) {

}

function insertHead($aHead) { 
    //require_once __DIR__.'./eccommon/php/autoload.php';
    //total hours spent here : 5. See autoload.php
    //something in autoload adds 1 extra empty line to response, making the xml invalid to the DLL.
    //require_once './config/ixsys_config.php';
    
    $Ixsys_config = new Ixsys_config();
    $dbc = $Ixsys_config->init_axalta_db();
    $client = "Nexsyis";
    foreach ($aHead as $key => $row) {     
        //return json_encode($row); 
        file_put_contents("headrow.log", json_encode($row['hinvc'])); 
        //Be sure the invoice is not already in the database
        $sql = "Select * from ecshead_web where hinvc = '".$row['hinvc']."' and hstore = '".$row['hstore']."'";    
        $result = $dbc->query($sql);
        //return $result->num_rows;
        if ($result->num_rows > 0) {
            return "1";//to reply that was added successfuly
        } else {
            $hinvc = $row['hinvc'];
            $hcust = $row['hcust'];
            $htime = $row['htime'];
            $hdate = $row['hdate'];
            $hisub = $row['hisub'];
            $hstax = $row['hstax'];
            $hdisc = $row['hdisc'];
            $hpo = (is_array($row['hpo']) ? "" : $row['hpo']); //if element is empty string then it is converted to empty array and shows up as "Array" in database
            $hstore = $row['hstore'];
            $hclerk = (is_array($row['hclerk']) ? "" : $row['hclerk']);
            $hcomm = (is_array($row['hcomm']) ? "" : $row['hcomm']);
            $client = "workdayJHCC";
            $jhccFtpExported = false;
            //Is a new invoice so go further
            $sql = "INSERT into ecshead_web (
                                            hinvc,
                                            hcust,
                                            htime,
                                            hdate,
                                            hisub,
                                            hstax,
                                            hdisc,
                                            hpo,
                                            hstore,
                                            hclerk,
                                            hcomm, 
                                            client,
                                            jhccFtpExported)
                                            VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?)"; 
            $stmt = $dbc->prepare($sql);            
            $stmt->bind_param('iisssssssssss', $hinvc, $hcust, $htime, $hdate, $hisub, $hstax, $hdisc, $hpo, $hstore, $hclerk, $hcomm, $client, $jhccFtpExported);
            if($stmt == false){
                //Some error occured
                $errmsg =  date("Y-m-d H:i:s")."   Some error occured in server.php function insertHead(): ".$dbc->error." SQL QUERY:".$sql."\r\n";
                writeToLog(date("Y-m-d H:i:s")." Error insering invoice head SQL run:".$sql." ");
                file_put_contents("ERROR_ENDPOINT.log", $errmsg,FILE_APPEND);
                mysqli_close($dbc);
                return $errmsg;
            }else{
                
                $res = $stmt->execute();
                //echo var_export($res,true);
            }

            if (!$res)
            {
                mysqli_close($dbc);
                return mysqli_stmt_error($stmt);
                return "Failure - Error updating order!";
            }
            return $res;
        }
    }
}



function insertTran($aTran)   {    
    //something in autoload adds 1 extra empty line to response, making the xml invalid to the DLL.
    //require_once __DIR__.'/eccommon/php/autoload.php';
    //require_once './config/ixsys_config.php';
    $Ixsys_config = new Ixsys_config();
    $dbc = $Ixsys_config->init_axalta_db();
    foreach ($aTran as $key => $row)    {
        file_put_contents("tranrow.log", json_encode($row['tinvc']));
        //return json_encode($row);
        $trandate = $row['tdate'];
        //return $trandate;
        //$trandate = date("Y-m-d H:i:s", strtotime($row['tdate']));  
		$tinvc = $row['tinvc'];
        $tcust = $row['tcust'];
        $ttyp = $row['ttyp'];
        $tqty = $row['tqty'];
        $tmfg = $row['tmfg'];
        $tpart = $row['tpart'];
        $tsize = $row['tsize'];
        $tprc = $row['tprc'];
        $tax = $row['tax'];
        $tdesc = $row['tdesc'];
        $tstore = $row['tstor'];
        $sql = "INSERT into ecstran_web (
                                            tinvc,
                                            tcust,
                                            tdate,
                                            ttyp,
                                            tqty,
                                            tmfg,
                                            tpart,
                                            tsize,
                                            tprc,
                                            tdesc,
                                            tax,
                                            tstore
                                        )
                                VALUES (?,?,?,?,?,?,?,?,?,?,?,?)";   
          $stmt = $dbc->prepare($sql);
                                    
        $stmt->bind_param('isssisssssss', $tinvc, $tcust, $trandate, $ttyp, $tqty, $tmfg, $tpart, $tsize, $tprc, $tdesc, $tax, $tstore);  
        if($stmt == false){
            //Some error occured
            $errmsg =  date("Y-m-d H:i:s")."   Some error occured in server.php function insertTran(): ".$dbc->error." SQL QUERY:".$sql."\r\n";
            writeToLog(date("Y-m-d H:i:s")." ERROR adding the component insertTran(): ".$sql);
            file_put_contents("ERROR_ENDPOINT.log", $errmsg,FILE_APPEND);
            mysqli_close($dbc);
            return $errmsg;
        }else{            
            
            $res = $stmt->execute();
            //echo var_export($res,true);
        }
    }
    mysqli_close($dbc); 
    return $res;
    
}

function insertCust($username, $password,$custno,	$bnam,	$bad1,	$bad2,	$bcty,	$bst,	$bzip,	$snam,	$sad1,	$sad2,	$scty,	$sst,	$szip,	$taxable,	$cstyp,	$bphone,	$sphone,	$mapsco)
    {
        //something in autoload adds 1 extra empty line to response, making the xml invalid to the DLL.
        //require_once __DIR__.'/eccommon/php/autoload.php';
        //require_once './config/ixsys_config.php';
        if (!doAuthenticateApplet($username, $password))
        {
            $error_message = " - Invalid SOAP username or password. Please check Soap Client credentials.";
            return json_encode($error_message);
        }
        require_once('mysqli_connect.php');   
        $Ixsys_config = new Ixsys_config();
        $dbc = $Ixsys_config->init_axalta_db();
		
		//Check if customer already exist
		$sql = "SELECT * FROM `ecscust` where custno = ".$custno;
		$stmt = $dbc->prepare($sql);
		$stmt->execute();
		$res = $stmt->get_result();
		$aCustomer = [];
		if ($res)   
		{ 
			$aCustomer = $res->fetch_all(MYSQLI_ASSOC);
		}else{
			$error_message = "Failure - Couldn't issue database query".mysqli_error($dbc);
			return "Error: ".$error_message; 
		}
		if(is_array($aCustomer) && !empty($aCustomer)){
			//Means the customer already exist!
			//TODO: when exist, update the database maybe if has changes
			$stmt->close();
			mysqli_close($dbc);
			return "Customer Already exist!";
		}
		
        $trandate = date("Y-m-d H:i:s", strtotime($tdate));  
		$tinvc = (int)$tinvc;
        $sql = "INSERT into ecscust (
									custno,	
									bnam,	
									bad1,	
									bad2,	
									bcty,	
									bst,	
									bzip,	
									snam,	
									sad1,	
									sad2,	
									scty,	
									sst,	
									szip,	
									taxable,	
									cstyp,	
									bphone,	
									sphone,	
									mapsco
                                    )
                            VALUES (
									$custno,	
									'$bnam',	
									'$bad1',	
									'$bad2',	
									'$bcty',	
									'$bst',	
									$bzip,	
									'$snam',	
									'$sad1',	
									'$sad2',	
									'$scty',	
									'$sst',	
									$szip,	
									$taxable,	
									$cstyp,	
									'$bphone',	
									'$sphone',	
									'$mapsco'
                                    )";   
                                    $stmt = $dbc->prepare($sql);
        $res = $stmt->execute();
		mysqli_close($dbc);
		
        return $res;
        
    }



function writeToLog($message = ""){
		$myfile = fopen("workday_log.txt", "a") or die("Unable to open file!");
		$txt = $message."\r\n";
		fwrite($myfile, $txt);
		fclose($myfile);
		
	}
?>