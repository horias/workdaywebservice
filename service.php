<?php
    require 'server.php';
    require 'lib/nusoap.php';
    $server = new nusoap_server();
    $server->soap_defencoding = 'UTF-8';
    $server->decode_utf8 = false;
    $server->encode_utf8 = true;
    $server->configureWSDL("ecWorkdayAzureService","urn:ecAzr");		
    $server->register(
                "insertHead", //name of function - previously insertInvoice
                array("username"=>"xsd:string","password"=>"xsd:string","hinvc"=>"xsd:string","hcust"=>"xsd:string","htime"=>"xsd:string","hdate"=>"xsd:string","hsub"=>"xsd:string","hstax"=>"xsd:string","hdisc"=>"xsd:string","hpo"=>"xsd:string","hstore"=>"xsd:string","hclerk"=>"xsd:string","hcomm"=>"xsd:string"), //inputs
                array("cResult"=>"xsd:string"), //outputs
                "",
                "",
                "rpc",
                "encoded",
                ""
                );
    $server->register(
                "insertTran", //name of function - previously update_ecstran_web
                array("username"=>"xsd:string","password"=>"xsd:string","tinvc"=>"xsd:string","tcust"=>"xsd:string","tdate"=>"xsd:string","ttyp"=>"xsd:string","tqty"=>"xsd:string","tmfg"=>"xsd:string","tpart"=>"xsd:string","tsize"=>"xsd:string","tprc"=>"xsd:string","tdesc"=>"xsd:string","tax"=>"xsd:string","orderId"=>"xsd:string", "tstore"=>"xsd:string"), //inputs
                array("cResult"=>"xsd:string") //outputs
    );
	$server->register(
                "insertCust", //name of function - previously update_customer
                array("username"=>"xsd:string","password"=>"xsd:string","custno"=>"xsd:string",		"bnam"=>"xsd:string",		"bad1"=>"xsd:string",		"bad2"=>"xsd:string",		"bcty"=>"xsd:string",		"bst"=>"xsd:string",		"bzip"=>"xsd:string",		"snam"=>"xsd:string",		"sad1"=>"xsd:string",		"sad2"=>"xsd:string",		"scty"=>"xsd:string",		"sst"=>"xsd:string",		"szip"=>"xsd:string",		"taxable"=>"xsd:string",		"cstyp"=>"xsd:string",		"bphone"=>"xsd:string",		"sphone"=>"xsd:string",		"mapsco"=>"xsd:string"), //inputs
                array("cResult"=>"xsd:string") //outputs
    );

    $server->register(
                "uploadInvoice", //name of function
                array("username"=>"xsd:string","password"=>"xsd:string","ecshead"=>"xsd:string","ecstran"=>"xsd:string"),
                array("cResult"=>"xsd:string") //outputs
    ); 

    $server->register(
                "generate_EDI_file", //name of function
                array("hinvc"=>"xsd:string", "hstore"=>"xsd:string"), //inputs       
                array("cResult"=>"xsd:string"), //outputs
                "",
                "",
                "rpc",
                "encoded",
                ""
                );				
	@$server->service(file_get_contents("php://input"));
?>