<?php

	
    function init_db()
    {
        $ini = parse_ini_file(__DIR__.'/../config/webservice.ini');
		
        DEFINE('DB_USER',$ini['sqlDbUsr']);
        DEFINE('DB_PASSWORD',$ini['sqlDbPwd']);
        DEFINE('DB_HOST', $ini['sqlDbHost']);
        DEFINE('DB_NAME', $ini['axaltaSqlDbName']);
        static $dbc;
		
        if ($dbc===NULL)
        {
            $dbc = mysqli_connect(DB_HOST,DB_USER,DB_PASSWORD,DB_NAME)
            OR die('Could not connect to MySQL ' . mysqli_connect_error($dbc));
			
        }
        return $dbc;
    }
?>